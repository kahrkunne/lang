def check_type(t1, t2):
    if t1 == t2: return True
    if isinstance(t2, E_Typeclass):
        return any([check_type(t1, x) for x in t2.types])
    else:
        return False

class E_Type: pass
class E_Typeclass:
    def __init__(self, *types):
        self.types = types
    def add(self, t):
        self.types.append(t)

class E_Symbol(E_Type, str):
    def __repr__(self):
        return super().__repr__()[1:-1]

class E_Environment(E_Type, dict): pass
class E_List(E_Type, list): pass
class R_Program(E_Type, str): pass
class R_Token(E_Type, str): pass
class E_String(E_Type, str): pass
class E_Integer(E_Type, int): pass
class E_Float(E_Type, float): pass

E_Number = E_Typeclass(E_Integer, E_Float)
E_Atom = E_Typeclass(E_Symbol, E_Number, E_String)
E_Expression = E_Typeclass(E_Atom, E_List)

