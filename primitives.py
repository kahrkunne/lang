from etypes import *
from e_eval import e_eval

def define(symbol, expr, env) -> E_List:
    if symbol in env: raise DefineException(f'{symbol} is already defined!')
    env[symbol] = e_eval(expr, env)
    print(env)
    return E_List()
