import re

from exceptions import *
from etypes import *
from utils import c_not_contains

def lex(input: R_Program) -> List[R_Token]:
    tokens = []
    cur_token = ''
    in_string = False

    for c in (input := iter(input.replace('\n',''))):
        if not in_string and c in ['(', ')', ' ']:
            tokens += [cur_token, c]
        elif c == '\\' and in_string:
            cur_token += next(input)
        elif c == '"':
            tokens.append(cur_token if (in_string := not in_string) else f'"{cur_token}"')
        else:
            cur_token += c

        if c in ['(', ')', '"', ' '] and not in_string:
            cur_token = ''

    return list(filter(c_not_contains(['', ' ']), tokens[1:] + [cur_token]))

def parse(tokens: List[R_Token]) -> E_Expression:
    if tokens.count('(') != tokens.count(')'):
        raise ReadException('Unmatched Parentheses')
    for t in tokens:
        if t == '(': break
        if t == ')': raise ReadException('Unmatched Parentheses')

    parse_tree = [[]]
    for t in tokens:
        head = parse_tree[-1]
        if t == '(': parse_tree.append([])
        elif t == ')': parse_tree[-2].append(parse_tree.pop())
        elif t[0] == '"': head.append(E_String(str(t[1:-1])))
        elif t.lstrip('-').isdigit(): head.append(E_Integer(int(t)))
        elif t.lstrip('-').replace('.','').isdigit(): head.append(E_Float(float(t)))
        else: head.append(E_Symbol(t))

    return parse_tree[0][0]

def read(program: R_Program) -> E_Expression:
    return parse(lex(program))
