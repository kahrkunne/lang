from etypes import *

def e_eval(expr: E_Expression, env: E_Environment) -> E_Expression:
    if isinstance(expr, E_List):
        return env[expr[0]](*expr[1:], env)
    elif isinstance(expr, E_Symbol):
        return env[expr]
    else:
        return expr
