import functools
import inspect
import operator
from string import ascii_lowercase

from etypes import *
from read import lex, parse
from e_eval import e_eval

def proper_name(f, signature=True):
    # FIXME probably a better way of doing this
    name = f.__name__
    if signature:
        name += str(inspect.signature(f))
    if f.__module__ != '__main__':
        name = f.__module__ + '.' + name

    return name

class Curry:
    # FIXME this doesn't work with kwargs at all as far as I can tell
    def __init__(self, f, num_args=None, name=None, signature=None, vals=[]):
        self.f = f
        self.signature = signature or inspect.signature(f)
        self.vals = vals

        if num_args is None:
            self.num_args = len(inspect.getfullargspec(f).args)
        else:
            self.num_args = num_args

        if name is None:
            self.name = proper_name(f, signature=False)
        else:
            self.name = name

    def __call__(self, *args, **kwargs):
        c = functools.partial(self.f, *args, **kwargs)
        if len(c.args) == self.num_args:
            return c()
        elif len(c.args) > self.num_args:
            # FIXME unfuck
            raise TypeError('{} takes {} positional argument{} but {} were given'.format(
                self.f, self.num_args, 's' if self.num_args != 1 else '', len(c.args)
            ))
        else:
            return Curry(c, self.num_args, self.name, self.signature, self.vals + list(args))
    def __repr__(self):
        return self.__name__
    @property
    def __name__(self):
        vals = iter(self.vals)
        args = iter(self.signature.parameters)
        name = '<curry ' + self.name + '('
        while True:
            try:
                v = next(vals)
                name += next(args) +'='+ v.__repr__() + ','
            except StopIteration:
                break

        name += ','.join(args)
        name += ')>'

        return name

def compose(*functions):
    spec = inspect.getfullargspec(functions[-1])
    num_args = len(spec.args)
    name = f"<compose {', '.join(proper_name(f, signature=False) for f in functions)})>"
    return Curry(functools.reduce(lambda f, g: lambda *x: f(g(*x)), functions, lambda x: x),
                 num_args=num_args, name=name, signature=inspect.signature(functions[-1]))

# Curried versions of all binary operators
# Equivalent to c_lt = Curry(operator.lt) etc
vars().update({f'c_{op}' : Curry(getattr(operator, op))
               for op in ['lt', 'le', 'eq', 'ne', 'ge', 'gt', 'is_not', 'add',
                          'floordiv', 'lshift', 'mod', 'mul', 'matmul', 'pow',
                          'rshift', 'sub', 'truediv', 'xor', 'concat', 'contains',
                          'countOf', 'delitem', 'indexOf', 'setitem', 'iadd',
                          'iand', 'iconcat', 'ifloordiv', 'ilshift', 'imod',
                          'imul', 'imatmul', 'ior', 'ipow', 'irshift', 'isub',
                          'itruediv', 'ixor']})

c_is = Curry(operator.is_)
c_and = Curry(operator.and_)
c_or = Curry(operator.or_)
c_not_contains = compose(operator.not_, operator.contains)

