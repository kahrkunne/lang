from etypes import *
from read import lex, parse
from e_eval import e_eval
import primitives

base_env = E_Environment({
    E_Symbol('define'): primitives.define,
})

def load_module(path: str) -> E_Environment:
    with open(path, 'r') as f:
        tree = parse(lex(f.read()))

    return e_eval(tree, base_env)
